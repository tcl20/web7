---
title: "CSR and Social Value"
description: ""
slug: "socialvalue"
image: "csr.jpg"
keywords: ""
categories: 
    - ""
    - ""
date: 2020-02-02T11:04:49+08:00
draft: false
---
Being accountable and responsible for our actions is paramount today. 

There is an increasing need for companies to reduce the impact they have on the world and foster local opportunities. Every company is different in the impact they make; however, The Comms Loop can assist with understanding the complexity of the environmental and social context in which you operate.

We can create a sustainability strategy focussed on your operations and help to embed this into your business strategy. We can support you in identifying stakeholders and mapping their needs.

We can help to create targets and policies and give sound advice on how to foster relationships in the communities in which you operate. Specialising in early careers, education and employment, The Comms Loop can create objectives that are meaningful and achievable. We have proven experience in delivering activity to support your targets and can advise on reporting.

[Say Hello](/sayhello) 

![logo](/img/small.jpg)
