---
title: "Events"
slug: "events"
image: "events.jpg"
date: 2020-02-02T11:04:49+08:00
 
draft: false
---
From internal to external events, we are experienced in planning a wide range of events. We can help to create engaging events that are tailored to your objectives and delivered on a range of budgets. 

We like to inspire and use innovative ways to involve your audience, whilst delivering the all-important messages. We can help to devise the strategy, create a theme, define your messages and create a truly rewarding and exciting experience. 

Events include reward and recognition events, colleague briefing sessions, media events, press opportunities, stakeholder engagement events, client events, community engagement events and charity events.

[Say Hello](/sayhello) 

![logo](/img/small.jpg)


