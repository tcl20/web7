---
title: "COVID-19"
date: 2020-03-23T11:04:49+08:00
draft: false
---
The  Comms Loop  is offering companies without  corporate  communications  support a free advice
service to help you communicate to your stakeholders during the ongoing COVID-19 crisis. 

We are specialised in communicating with customers, employees and other groups of people who
are invested in your company and want to offer our support during these challenging times. 

If you think we can help in any way, please contact us to have a chat. Please stay safe everyone. 

Kind regards

Claire Allcoat

Managing Director


[Say Hello](/sayhello) 

![logo](/img/small.jpg)
