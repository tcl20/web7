---
title: "Your Data"
date: 2020-02-02T11:04:49+08:00
draft: false
---

# Privacy Policy

1 This privacy policy applies to The Comms Loop’s website at thecommsloop.com (the “Website”). This policy is effective from 25 March 2020.

2 We take your privacy and our legal obligations seriously. This policy has been prepared to ensure that our collection, processing and other use of personal data complies with our obligations under the Data Protection Act 1998 (“DPA”) and the General Data Protection Regulations (“GDPR”).

3 For the purpose of the DPA and GDPR, we are the data controller and any enquiry regarding the collection or processing of your data should be addressed to us via hello@thecommsloop.com.

4 By using the Website you consent to this policy. 

5 Should we ask you to provide any information by which you can be identified when using this website, it will only be used in accordance with this privacy policy. We may collect your name, job title and your contact information. 

6  The information that we collect and store relating to you is used to improve our services and for us to contact you. For example, we may use the information to notify you about changes to our website. 

7 If you do not want us to use your data, you will have the opportunity to withhold your consent to this. If you have given us consent to use your data for a particular purpose you can revoke or vary that consent at any time, by emailing us to advise us of your requirements.

8 The information you provide to us may be transferred to and stored in countries outside of the European Economic Area (EEA) as we use remote website server hosts. It may also be processed by staff operating outside the EEA who work for one of our suppliers.

9 We are committed to ensuring that your information is secure. In order to prevent unauthorised access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online. The transmission of information via the internet or mail is however not completely secure, and we cannot guarantee the security of data while you are transmitting it to our site.

10  We may disclose your information to regulatory bodies to enable us to comply with the law.
Otherwise, we will process, disclose or share your personal data only if required to do so by law or in the good faith belief that such action is necessary to comply with legal requirements or legal process served on us or the website.

11 You might find links to third party websites on our website. These websites have their own privacy policies, which you should check. We do not accept any responsibility or liability for their data security.

12 Our website uses cookies to provide statistical information regarding the use of our website. This data does not identify any personal details whatsoever, but helps us to improve our website. 

13  The DPA and GDPR give you the right to access information held about you by us. Please contact us by email if you wish to request confirmation of what personal information we hold relating to you. There is no charge for requesting that we provide you with details of the personal data that we hold. We will provide this information within one month of your requesting the data.

14  You have the right to change the permissions that you have given us in relation to how we may use your data. You also have the right to request that we cease using your data or that we delete all personal data records that we hold relating to you. You can exercise these rights at any time by email.

15 We may update this policy to reflect changes to our website and customer feedback. 

![logo](/img/small.jpg)