---
title: "Furloughed staff: keeping connected"
description: ""
slug: "furloughcomms"
image: "commfurlough.jpg"
keywords: ""
categories: 
    - ""
    - ""
date: 2020-04-24T22:26:09-05:00
draft: false
---

Over recent weeks, I’ve been chatting with people who have been placed onto the Government’s job retention scheme, also known as the furlough scheme, and given the restrictions around communications, I wondered what their thoughts were on the current situation.
A couple of things stood out to me: besides a reduction in income, their biggest concerns were, unsurprisingly, based around job security and benefits like annual leave accrual and whether these would be impacted. The majority had sought reassurance and support from their leaders or HR function. 

Another was that for the most part, they wanted to know how the business was performing financially or how long their organisation could remain closed and still retain their staff members. Most companies were providing updates, albeit via opt in services and to personal email addresses or WhatsApp groups. 

Of the people I have spoken to, most of their companies were providing information on coronavirus and the furlough scheme via FAQ’s. Information on Health and wellbeing, whether it be fitness related or online courses on mental health were also being regularly issued.

So, what did they still want to know? Naturally, an end date and the process for returning to work were at the top of the list, but some wanted to know who was still working in their company, so they could praise them for helping to keep the business running – what a great thing to do.

Every person had read updates and information on their company website and social media pages, showing how important it is for consistent, informative updates which are suitable for all audiences.

These are unprecedented circumstances in many ways and communications is certainly one of them. Furloughing staff isn’t done so much in the UK, and therefore the restrictions around what you can and can’t do is new territory. However, we know there is still a desire from staff members for regular communication and even more so as you prepare to return to full operations. 

If you need any support on communications during this pandemic or for any future projects, please don’t hesitate to contact us at hello@thecommsloop.com. Additionally, the CIPR have produced a useful one pager on communication with furloughed members of staff, so check it out for more information. 

[Say Hello](/sayhello) 

![logo](/img/small.jpg)
