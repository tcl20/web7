---
title: "Returning to the 'new' normal"
description: ""
slug: "newnormal"
image: "newnormal.jpg"
keywords: ""
categories: 
    - ""
    - ""
date: 2020-05-03T16:26:09-05:00
draft: false
---

Everyone is talking about the ‘new’ normal. We don’t yet know what it’s going to look like and for how long it’s going to continue, but we do know that life as we know it is going to change. 

Companies across the globe are currently assessing how their company works and how they can return to full operations safely. Not only this, but they might be looking at other ways of working – will they be looking at introducing more agile working and will WFH play more of a part in everyone’s working week?

With this in mind and with lots of discussions around returning to work, it’s even more important to be looking at your communication strategies with more focus than ever before. Stakeholders are naturally going to want reassurance that you’ve put practices into place for a safe return to what will be the ‘new’ normal. They will be looking for clear, planned and truthful communications.

It’s essential to gain trust from anyone who will access your business - whether that be a customer, a supplier or an employee - that the environment is safe. Processes are likely to have changed and most businesses will have differing procedures, so it’s important to ensure that the messages which are specific to your business are clear and communicated at the right time.  It’s also important to look wider than your process changes - have you had to adapt your business strategy and focus as a result of economic impact on your company? Do you need to re-educate your stakeholders on your new narrative?

Engaging your employees is vital and keeping morale high will be key. Managing change is difficult in any situation and in unsure times, communication needs to be at its clearest. Employees will look to their leaders for confidence and comfort that things are manageable. Empowering your leaders and line managers will help to deliver the right messages at the right time. It's easy for a company to use a broadcast method across all internal channels when trying to deliver a message in a crisis but choosing the right channel will deliver better outcomes and your workforce will feel more informed. As always, listening and two-way communication should take precedence and asking for input into decisions throughout will have positive results – working and listening groups can have great results. 

Although engaging your employees and customers will be a priority, other important stakeholder groups such as suppliers, investors and the local community will also be looking for reassurance and information. A robust integrated communications strategy will help you to communicate clearly, maintain your relationships and strengthen your brand.

So, as we all prepare for the ‘new’ normal, now is the time to make sure your communications strategy supports you.

If you think we can help with your communication strategies or we can help with any future projects, please get in touch at hello@thecommsloop.com. 


[Say Hello](/sayhello) 

![logo](/img/small.jpg)


