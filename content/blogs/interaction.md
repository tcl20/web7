---
title: "Social Interaction - it's in our DNA"
description: ""
slug: "interaction"
image: "interaction.jpg"
keywords: ""
categories: 
    - ""
    - ""
date: 2020-04-23T22:26:09-05:00
draft: false
---

Humans are intrinsically social beings and work is no exception. Whilst we’re experiencing lockdown due to the coronavirus pandemic, social interaction with our work colleagues who want to catch up with their co-workers is still very important. Lately, I’ve seen some great examples, from quiz nights and online games to virtual coffee breaks and breakfast clubs. 

Colleagues all over the world are missing face to face interactions with their managers and peers. We’re so used to having interactions at work on a daily basis and just because we aren’t chatting while making a coffee or over lunch doesn’t mean that those exchanges should disappear altogether. 

Benefits to social interactions with remote workers can be extensive with reduced feelings of isolation, increased engagement and boosted morale.

It got me thinking about what else we could do, so here are some ideas which can help: 

• Home fitness competitions

• Virtual notice boards, where people post things of interest, like recipe ideas for lockdown lunches with the kids

• Virtual book clubs

• Shared reward and recognition session
 
• Online games, such as scrabble 
    
There are so many ways that we can still engage with remote teams, so why not get creative. 

[Say Hello](/sayhello) 

![logo](/img/small.jpg)
