---
title: "WFH"
description: ""
slug: "wfh"
image: "WFH.jpg"
keywords: "wfh"
categories: 
    - ""
    - ""
date: 2020-04-26T21:26:09-05:00
draft: false
---

Working From Home (WFH) used to be such a taboo. Someone in the office would announce they’re working from home tomorrow. Two things would then happen: first, most people would say, “how didn’t I know that was a thing?” and secondly, it was presumed they would be doing the bare minimum whilst catching up on their favourite tv programme.

Of course, this was when working from home didn’t really happen, but over the last few years it’s become much more widely acceptable and not just if you’re expecting a workman. 

Amid the current pandemic, many more of us are working from home currently and temporary home offices have become the norm. There’s lots of advice online, such as establishing boundaries, creating a routine and not forgetting to be sociable. There’s also some great software solutions to help with making communication easier and more personable, such as Microsoft Teams, Slack and Zoom. 

Flexible working can bring huge benefits. Employees can be more empowered, engaged, and productive, in turn reducing absenteeism and staff turnover. Health and wellbeing benefits can make a significant difference to people and their work life balance. New technology can easily make a virtual office seem like a reality. Undeniably, there are disadvantages to working from home, but by adopting clear processes, clear management and an understanding of how to retain a good level of staff engagement, these can be easily mitigated. 

At the moment, we don’t know what the short or long-term changes to our normal working day may be, but whatever is coming our way, it’s good to know that we’re easily adaptable and can rise to the challenge. 

[Say Hello](/sayhello) 

![logo](/img/small.jpg)


